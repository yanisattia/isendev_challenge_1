#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <time.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_mixer.h>

#include "engine.h"
#include "utils/utils.h"
#include "states/states.h"

#include "gameObjects/planet.h"
#include "gameObjects/solar_system.h"
#include "gameObjects/stars.h"
#include "gameObjects/ui.h"

/*  
 * Initialisation d'une structure de type engineT
 * Cette structure contiendra toutes les variables 
 * de l'environnement graphique et calculatoire du
 * programme.
*/
engineT engine;

//Tableau contenant les ressources graphiques du programme
imageT *gfxAssets[NBR_GFXASSETS];

//Police de caractère + textes
TTF_Font *font[10] = {NULL};
textT *gameText[10] = {NULL};
gameDataT *gameData[5] = {NULL};

//Background music
Mix_Music *music[2] = {NULL};
Mix_Chunk *mouseSound = NULL;

/* FRAME COUNTER */
clock_t start_timer;
clock_t end_timer;
double uptime = 0;
double elapsed = 0.0;
double fps = 0.0;
int frame;
double fpsLimit = 60;
int fpsCap = 1;
textT *fpsText;
/* END OF FRAME COUNTER */

int main(int argc, char *argv[]){
    UNUSED(argc);
    UNUSED(argv);
    //Appel de la fonction d'initialisation
    init();

    engine.render = getRendererData();
        
    if(argc > 1){
        if(strcmp(argv[1], "reset") == 0){
            for(int i = 0; i < 5; ++i){
                resetSaves(i);
            }
        }
    }

    while(engine.process){
        start_timer = clock();

        update();
        draw();
        
        //mesure d'une boucle
        end_timer = clock();
        elapsed = (end_timer - start_timer)*1000.f/CLOCKS_PER_SEC;

        /* FRAME LIMITER / COUNTER */
            //Incrémentation du temps d'exec jusqu'a une seconde
            //Sauvegarde du nombre de frame = frame pour une seconde.
            //RAZ puis remesure pour une seconde
            if(uptime < 1000){
                uptime += elapsed;
                if(uptime >= 1000){
                    fps = frame;
                    uptime = 0;
                    frame = 0;
                }
            }
            if(fpsCap == 1 && (elapsed < 1000/fpsLimit)) { 
                //Sleep the remaining frame time 
                SDL_Delay((1000/fpsLimit) - elapsed); 
                uptime += (1000/fpsLimit) - elapsed;
                if(uptime >= 1000){
                    fps = frame;
                    uptime = 0;
                    frame = 0;
                }  
            }

            SDL_DestroyTexture(fpsText->textTexture);
            SDL_FreeSurface(fpsText->textSurface);
            free(fpsText->string);
            char string[255];
            sprintf(string, "%.0lf", fps);
            strcat(string, " fps");
            fpsText = newText(font[1], string, 255, 255, 255, 255);
            //Affichage de la position de la souris
            //printf("\rMouse (%d %d)\n\033[A", engine.mousePos.x, engine.mousePos.y);
            //Affichage du compteur de fps et du nombre de frames dpuis le début de l'execution du programme.
            //printf("\rFPS : %lf | cap (%lf) : %d | frame %lf\n\033[A", fps, fpsLimit, fpsCap, frameCounter);
        /* END OF FRAME LIMITER / COUNTER */
    }

    end();

    return 0;
}

void init(){
    /* Tant que engine.process == true; Le programme continue de s'éxecuter */
    engine.process = true;
    engine.loading = 0;
    engine.loaded = 0;
    engine.render = getRendererData();

    engine.ui.elements = NULL;
    engine.new_game.elements = NULL;
    engine.load_game.elements = NULL;
    engine.options_menu.elements = NULL;

    /* Creating renderer */
    createRenderer("Odyssey");
    /* Initialising random number generator */
    srand(time(NULL)); 

    /* Initialising frame counter */
    frame = 0;
    engine.frameCounter = 0;
    /* init ttf */
    TTF_Init();
    for(int i = 0; i < 10; ++i){
        font[i] = TTF_OpenFont("data/assets/font.ttf", 10*(i+1));
    }
    engine.displayFps = 1;
    fpsText = newText(font[0], "0", 255, 255, 255, 255);

    if(engine.render->fpsCap <= 0)
        fpsCap = 0;
    else{
        fpsCap = 1;
        fpsLimit = engine.render->fpsCap;
    }

    /* init mixer */
    if(Mix_OpenAudio(MIX_DEFAULT_FREQUENCY, MIX_DEFAULT_FORMAT, MIX_DEFAULT_CHANNELS, 1024) == -1) 
    { 
        return; 
    }
    music[0] = Mix_LoadMUS("data/assets/menu_music.mp3");
    mouseSound = Mix_LoadWAV("data/assets/mouse.wav");
}

void update(void){
    /* Code qui sera executé en boucle tant que le programme n'est pas terminé */
    /* Ici, on appelle les différentes fonctions d'actualisation selon l'état actuel du programme */
    SDL_GetMouseState(&(engine.mousePos.x), &(engine.mousePos.y));

    if(engine.event.type == SDL_QUIT){
        setState(CLOSE);
    }

    switch(getState()){
        case BOOT:
            bootUpdate();
        break;

        case MENU:
            menuUpdate();
        break;

        case GAME:
            gameUpdate();
        break;

        case CLOSE:
            closeUpdate();
        break;
    }
}

void draw(){
    /* Code executé lui aussi en boucle, mais cette fois-ci, consacré à l'affichage */
    /* On règle la couleur de dessin sur du bleu foncé (très très foncé) sans transparence */
    SDL_SetRenderDrawColor(getRenderer(), 0, 0, 15, 0);
    /* Nettoyage de la fenêtre et remplacement du fond par la couleur de dessin actuelle */
    SDL_RenderClear(getRenderer());

    /*  Code contenant les instructions de dessin ici */
    /* Ici, on appelle les différentes fonctions d'actualisation selon l'état actuel du programme */
    switch(getState()){
        case BOOT:
            bootDraw();
        break;

        case MENU:
            menuDraw();
        break;

        case GAME:
            gameDraw();
        break;

        case CLOSE:
            closeDraw();
        break;
    }
    /* Fin du code contenant les instructions de dessin */

    /* 
     * Les instructions données plus haut seront gardées en mémoire
     * Et ce, jusqu'à la prochaine actualisation via SDL_RenderPresent()
    */
    SDL_RenderPresent(getRenderer());

    // Incrémentation du compteur de fps à chaque frame.
    ++frame;
    ++engine.frameCounter;
}

void end(){
    /* Code à executer une seule fois, en fin de programme */
    destroyRenderer();
    for(int i = 0; i < NBR_GFXASSETS; ++i){
        destroyImage((gfxAssets[i]));
    }
    for(int i = 0; i < 10; ++i)
        TTF_CloseFont(font[i]);
    TTF_Quit();
    Mix_FreeMusic(music[0]);
}

void loadingScreen(int val){
    SDL_SetRenderDrawColor(getRenderer(), 0, 0, 15, 0);
    SDL_RenderClear(getRenderer());
    char valString[5];
    char string[255];
    sprintf(valString, "%d", val);
    strcpy(string, "Now Loading... ");
    strcat(string, valString);
    strcat(string, "%");
    textT *text = newText(font[2], string, 255, 255, 255, 255);
    displayText(text, newVector2(50, 50));
    SDL_RenderPresent(getRenderer());
    SDL_FreeSurface(text->textSurface);
    SDL_DestroyTexture(text->textTexture);
    free(text);
}
/*** GAME DATA ***/

gameDataT *createGameData(int slot){
    gameDataT *gameData = (gameDataT *) malloc(sizeof(gameDataT));
    gameData->empty = true;
    gameData->slot = slot;
    gameData->nb_systems = 0;
    gameData->systems = NULL;
    return gameData;
}

void saveGameData(gameDataT *gameData){
    FILE *f = NULL;
    char path[255], tmp[255];
    strcpy(path, "data/saves/slot_");
    sprintf(tmp, "%d", gameData->slot);
    strcat(path, tmp);
    strcat(path, "/gamedata.sav");
    for(int i = 0; i < gameData->nb_systems; ++i){
        saveSolarSystem(&(gameData->systems[i]), gameData->slot, i);
    }
    f = fopen(path, "wb");
        fwrite(gameData, sizeof(gameDataT), 1, f);
    fclose(f);
}

gameDataT *loadGameData(int slot){
    gameDataT *gameData = (gameDataT *) malloc(sizeof(gameDataT));
    FILE *f = NULL;
    char path[255], tmp[255];
    strcpy(path, "data/saves/slot_");
    sprintf(tmp, "%d", slot);
    strcat(path, tmp);
    strcat(path, "/gamedata.sav");
    f = fopen(path, "rb");
        fread(gameData, sizeof(gameDataT), 1, f);
    fclose(f);

    return gameData;
}

void clearGameData(gameDataT *gameData){
    for(int i = 0; i < gameData->nb_systems; ++i){
        free(gameData->systems[i].planetes);
    }
    free(gameData->systems);
    free(gameData);
}

void loadSystems(gameDataT *gameData){
    gameData->systems = (solarSystemT *) malloc(sizeof(solarSystemT)*gameData->nb_systems);
    for(int i = 0; i < gameData->nb_systems; ++i){
        loadSolarSystem(&(gameData->systems[i]), gameData->slot, i);
    }
}

void addSolarSystem(gameDataT *gameData){
    if(gameData->nb_systems == 0){
        gameData->nb_systems = 1;
        gameData->systems = (solarSystemT *) malloc(sizeof(solarSystemT));
        newSolarSystem(&(gameData->systems[0]));
    }else if(gameData->nb_systems > 0){
        gameData->nb_systems++;
        gameData->systems = (solarSystemT *) realloc(gameData->systems, sizeof(solarSystemT)*gameData->nb_systems);
        newSolarSystem(&(gameData->systems[gameData->nb_systems-1]));
    }
    gameData->empty = false;
    saveGameData(gameData);
}

void resetSaves(int i){
    char path[255];
    char tmp[255];
    char cmd[255];
    char sprTmp[10];
    strcpy(path, "data/saves/slot_");
    sprintf(sprTmp, "%d", i);
    strcpy(tmp, path);
    strcat(tmp, sprTmp);
    strcpy(cmd, "rm ");
    strcat(tmp, "/");
    strcat(cmd, tmp);
    strcat(cmd, "*");
    system(cmd);

    gameDataT *gameData = createGameData(i);
    saveGameData(gameData);
    clearGameData(gameData);
}