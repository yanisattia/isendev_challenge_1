#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include "utils.h"

void newTileMap(tilemapT *tilemap, int size){
    tilemap->mapSize = size;
    tilemap->tiles = (tileT *) malloc(sizeof(tileT)*tilemap->mapSize);
}

void destroyTileMap(tilemapT *tilemap){
    free(tilemap->tiles);
}