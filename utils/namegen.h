#ifndef __NAMEGEN_H
#define __NAMEGEN_H

typedef char string[255];

void generateName(string buffer, string syllabesList[], int listSize, int nameSize);

#endif //__NAMEGEN_H