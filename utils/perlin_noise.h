#ifndef __PERLIN_NOISE_H
#define __PERLIN_NOISE_H

typedef struct perlinNoiseT perlinNoiseT;

struct perlinNoiseT{
    int size;
    int nOctaves;
    float *randInput;
    float *perlinOutput;
};

void newPerlinNoise(perlinNoiseT *perlinNoise, int size, int nOctaves);

void generateNoise(perlinNoiseT *perlinNoise);
//Fonction qui retourne aléatoirement une valeur flottante entre 0.0 et 1.0
float noise(void);

//Fonction qui calcule les valeurs du bruit de perlin et les stocke dans un tableau perlinVal
void perlinNoise1(perlinNoiseT *perlinNoise, int nOctaves);

#endif //__PERLIN_NOISE_H