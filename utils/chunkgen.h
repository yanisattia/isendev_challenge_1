#ifndef __CHUNKGEN_H
#define __CHUNKGEN_H

typedef struct chunkT chunkT;
typedef struct mapT mapT;

struct mapT{
    chunkT *chunkMap;
    chunkT *leftChunkMap;
    int chunkSize;
    int nbChunks;

    tilemapT *tilemap;

    int cursor; //The current chunk
};

struct chunkT{
    perlinNoiseT content;
    int position;
    int id;
    int generated;
};

void newMap(mapT *map, int nb, int size);
void destroyMap(mapT *map);
void newChunk(mapT *map);
void generateTileMap(mapT *map, int id);
void increaseSize(mapT *map);

//GENERIC CODE FOR CHUNK GENERATION AND CAMERA USAGE
/*
Init
mapT *map = newMap();
newChunk();

Loop
if(map.chunkMap[(camera->x/32)/map.chunkSize].generated){
    verticalTracking(camera, map.chunkMap[(camera->x/32)/map.chunkSize].content.perlinOutput[(camera->x/32)%map.chunkSize]);
}
//Auto generation of world
if(camera->x >= (map.nbChunks-1)*map.chunkSize*30){
    //printf("GENERATING CHUNK\n");
    newChunk(&map);
}
*/

//GENERIC CODE FOR DRAWING CHUNKS
/*
Drawing loop
for(int i = 0; i < map.nbChunks; ++i){
        if(map.chunkMap[i].generated){
            if(map.chunkMap[i].generated){
                for(int j = 0; j < map.chunkSize; ++j){
                    displayImage(&(map.tilemap[i].tiles[j].texture), "tile");
                }
            }
        }
    } 
*/

#endif //__CHUNKGEN_H