#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include "utils.h"

void newMap(mapT *map, int nb, int size){
    map->chunkSize = size;
    map->nbChunks = nb;
    map->cursor = 0;

    map->chunkMap = (chunkT *) malloc(sizeof(chunkT) * map->nbChunks);
    map->tilemap = (tilemapT *) malloc(sizeof(tilemapT) * map->nbChunks);
    for(int i = 0; i < map->nbChunks; ++i){
        map->chunkMap[i].position = map->chunkSize*i;
        map->chunkMap[i].id = i;
        map->chunkMap[i].generated = 0;
        newTileMap((map->tilemap), map->chunkSize);
    }
}

void destroyMap(mapT *map){
    map->nbChunks = 0;
    map->chunkSize = 0;
    free(map->chunkMap);
}

void newChunk(mapT *map){
    newPerlinNoise(&(map->chunkMap[map->cursor].content), map->chunkSize, 10);
    generateNoise(&(map->chunkMap[map->cursor].content));
    perlinNoise1(&(map->chunkMap[map->cursor].content), 10);

    for(int i = 0; i < map->chunkSize; ++i)
        map->chunkMap[map->cursor].content.perlinOutput[i]*=500;
    if(map->cursor != 0){
        double offset = map->chunkMap[map->cursor].content.perlinOutput[0]-map->chunkMap[map->cursor-1].content.perlinOutput[map->chunkSize-1];
        //printf("%lf\n", offset);
        for(int i = 0; i < map->chunkSize; ++i){
            map->chunkMap[map->cursor].content.perlinOutput[i]-=offset;
        }
    }

    map->chunkMap[map->cursor].generated = 1;
    generateTileMap(map, map->cursor);

    ++map->cursor;
    if(map->cursor == map->nbChunks)
        increaseSize(map);
}

void generateTileMap(mapT *map, int id){
    if(map->chunkMap[id].generated){
        for(int j = 0; j < map->tilemap[id].mapSize; ++j){
            map->tilemap[id].tiles[j].pos.x = (32*id*map->chunkSize)+32*j;
            map->tilemap[id].tiles[j].pos.y = (map->chunkMap[id].content.perlinOutput[j]);
            map->tilemap[id].tiles[j].texture = newTexture((gfxAssets[6]), map->tilemap[id].tiles[j].pos, newVector2(32, 32));
        }
    }
}

void increaseSize(mapT *map){
    map->nbChunks += 1;
    map->chunkMap = (chunkT *) realloc(map->chunkMap, sizeof(chunkT)*map->nbChunks);
    map->tilemap = (tilemapT *) realloc(map->tilemap, sizeof(tilemapT)*map->nbChunks);

    int i = map->nbChunks-1;

    map->chunkMap[i].position = map->chunkSize*i;
    map->chunkMap[i].id = i;
    map->chunkMap[i].generated = 0;

    map->tilemap[i].mapSize = map->tilemap[i-1].mapSize;
    map->tilemap[i].tiles = (tileT *) malloc(sizeof(tileT)*map->tilemap[i].mapSize);
}