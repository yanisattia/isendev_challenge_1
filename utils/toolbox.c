#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>

#include "utils.h"

vector2 newVector2(int x, int y){
    vector2 output;
        output.x = x;
        output.y = y;
    return output;
}

void getScreenCoordinates(SDL_Rect *screen, int x, int y){
    SDL_Rect *camera = getCamera();
    screen->x = (x - camera->x) * camera->w;
    screen->y = (y - camera->y) * camera->h;
}

void getScreenZoom(SDL_Rect *screen, int w, int h){
    SDL_Rect *camera = getCamera();
    screen->w = w * camera->w;
    screen->h = h * camera->h;
}

void zoom(SDL_Rect *camera){
    vector2 winSize;
    SDL_GetWindowSize(getWindow(), &(winSize.x), &(winSize.y));
    if(camera->w < 2 && camera->h < 2){
        camera->w*=2;
        camera->h*=2;
        camera->x += (winSize.x/camera->w) / 2;
        camera->y += (winSize.y/camera->h) / 2;
    }
}

void dezoom(SDL_Rect *camera){
    vector2 winSize;
    SDL_GetWindowSize(getWindow(), &(winSize.x), &(winSize.y));
    if(camera->w > 1 && camera->h > 1){
        camera->x -= (winSize.x/camera->w) / 2;
        camera->y -= (winSize.y/camera->h) / 2;
        camera->w/=2;
        camera->h/=2;
    }
}

void verticalTracking(SDL_Rect *camera, int posY){
    vector2 winSize;
    SDL_GetWindowSize(getWindow(), &(winSize.x), &(winSize.y));
    camera->y = (posY) - (winSize.y/camera->h)/2;
}

void horizontalTracking(SDL_Rect *camera, int posX){
    vector2 winSize;
    SDL_GetWindowSize(getWindow(), &(winSize.x), &(winSize.y));
    camera->x = (posX) - (winSize.x/camera->w)/2;
}

imageT *newImage(char *imgPath){
    imageT *image;
    image = (imageT *) malloc(sizeof(imageT));
    printf("# Loading image asset %s\n", imgPath);
    image->imgS = IMG_Load(imgPath);
    if(!(image->imgS)) {
        printf("IMG_Load: %s\n", IMG_GetError());
        // handle error
    }
    image->texture = SDL_CreateTextureFromSurface(getRenderer(), image->imgS);
    SDL_QueryTexture(image->texture, NULL, NULL, &(image->size.x), &(image->size.y));
    image->pos.x = 0;
    image->pos.y = 0;
    image->alpha = 255;

    return image;
}

imageT *newTexture(imageT *image, vector2 pos, vector2 size){
    imageT *texture;
    texture = (imageT *)malloc(sizeof(imageT));

    texture->pos.x = pos.x;
    texture->pos.y = pos.y;
    texture->size.x = size.x;
    texture->size.y = size.y;
    texture->alpha = 255;
    
    texture->imgS = SDL_ConvertSurface(image->imgS, image->imgS->format, SDL_SWSURFACE);
    texture->texture = SDL_CreateTextureFromSurface(getRenderer(), texture->imgS);
    SDL_QueryTexture(texture->texture, NULL, NULL, &(texture->size.x), &(texture->size.y));
    SDL_SetTextureBlendMode(texture->texture, SDL_BLENDMODE_BLEND);
    //printf("Texture created\n");

    return texture;
}

SDL_Surface *optimizeSurface(SDL_Surface *surf){
    SDL_Surface *windowSurface = SDL_GetWindowSurface(getWindow());
    SDL_Surface *opt = SDL_ConvertSurface(surf, windowSurface->format, 0);
    if (opt)
    {
        //printf("Optimized ! \n");
        SDL_FreeSurface(surf);
        return opt;
    }
    return surf;
} 

void destroyImage(imageT *image){
    SDL_FreeSurface(image->imgS);
    SDL_DestroyTexture(image->texture);
}

void moveImage(imageT *image, vector2 pos, vector2 size){
    if(pos.x != -1){
        image->pos.x = pos.x;
        image->pos.y = pos.y;
    }

    if(size.x != -1){
        image->size.x = size.x;
        image->size.y = size.y;
        if(size.x == 0) SDL_QueryTexture(image->texture, NULL, NULL, &(image->size.x), NULL);
        if(size.y == 0) SDL_QueryTexture(image->texture, NULL, NULL, NULL, &(image->size.y));
    }
}

void displayImage(imageT *image, char *name){
    SDL_Rect rect;
    rect.x = image->pos.x;
    rect.y = image->pos.y;
    rect.w = image->size.x;
    rect.h = image->size.y;
    SDL_SetTextureAlphaMod(image->texture, image->alpha);
    //printf("%d %d | %d %d\n", rect.x, rect.y, rect.w, rect.h);
    getScreenCoordinates(&rect, rect.x, rect.y);
    getScreenZoom(&rect, rect.w, rect.h);
    SDL_RenderCopy(getRenderer(), image->texture, NULL, &rect);
    //printf("Rendering object %s\n", name);
    UNUSED(name);
}

void drawPixel(int x, int y){
    SDL_Rect position;
    getScreenCoordinates(&position, x, y);
    SDL_RenderDrawPoint(getRenderer(), position.x, position.y);
}

void drawCircle(int xc, int yc, int r){
    int x = r-1;
    int y = 0;
    int tx = 1;
    int ty = 1;
    int err = tx - (r << 1); //Shift bit left by one = double value. (tx - diameter)

    while(x >= y){
        drawPixel(xc+x, yc-y);
        drawPixel(xc+x, yc+y);
        drawPixel(xc-x, yc-y);
        drawPixel(xc-x, yc+y);
        drawPixel(xc+y, yc-x);
        drawPixel(xc+y, yc+x);
        drawPixel(xc-y, yc-x);
        drawPixel(xc-y, yc+x);

        if(err <= 0){
            y++;
            err += ty;
            ty += 2;
        }
        if(err > 0){
            x--;
            tx += 2;
            err += tx - (r << 1);
        }
    }
}

textT *newText(TTF_Font *font, char *string, int r, int g, int b, int a){
    textT *text;
    text = (textT *)malloc(sizeof(textT));

    text->color.r = r;
    text->color.g = g;
    text->color.b = b;
    text->color.a = a;
    text->string = (char *) malloc(sizeof(char) * strlen(string));
    strcpy(text->string, string);
    text->textSurface = TTF_RenderText_Solid(font, text->string, text->color);
    text->textTexture = SDL_CreateTextureFromSurface(getRenderer(), text->textSurface);
    SDL_SetTextureBlendMode(text->textTexture, SDL_BLENDMODE_BLEND);

    return text;
}

void displayText(textT *text, vector2 pos){
    SDL_Rect dst;
    dst.x = pos.x;
    dst.y = pos.y;
    SDL_QueryTexture(text->textTexture, NULL, NULL, &(dst.w), &(dst.h));
    text->size.x = dst.w;
    text->size.y = dst.h;
    text->pos.x = pos.x;
    text->pos.y = pos.y;
    SDL_SetTextureAlphaMod(text->textTexture, text->color.a);
    SDL_RenderCopy(getRenderer(), text->textTexture, NULL, &dst);
}

void refreshText(textT *text, TTF_Font *font, char *string){
    SDL_FreeSurface(text->textSurface);
    SDL_DestroyTexture(text->textTexture);
    strcpy(text->string, string);
    text->textSurface = TTF_RenderText_Solid(font, text->string, text->color);
    text->textTexture = SDL_CreateTextureFromSurface(getRenderer(), text->textSurface);
}

void destroyText(textT *text){
    SDL_FreeSurface(text->textSurface);
    SDL_DestroyTexture(text->textTexture);
}