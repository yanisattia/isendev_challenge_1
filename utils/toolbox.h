#ifndef __TOOLBOX_H
#define __TOOLBOX_H

#include <SDL2/SDL_ttf.h>

/* Struct that contains a vector on 2D */
typedef struct vector2 vector2;
struct vector2{
    int x, y;
};

typedef struct imageT imageT;
struct imageT{
    vector2 pos;
    vector2 size;
    char *name;
    SDL_Texture *texture;
    SDL_Surface *imgS;
    int alpha;
};

typedef struct textT textT;

struct textT{
    SDL_Surface *textSurface;
    SDL_Texture *textTexture;
    SDL_Color color;
    vector2 pos, size;
    char *string;
};

vector2 newVector2(int x, int y);
void getScreenCoordinates(SDL_Rect *screen, int x, int y);
void getScreenZoom(SDL_Rect *screen, int w, int h);

SDL_Surface *optimizeSurface(SDL_Surface *surf);

imageT *newImage(char *imgPath);
void destroyImage(imageT *image);

void moveImage(imageT *image, vector2 pos, vector2 size);
void displayImage(imageT *image, char *name);

void drawPixel(int x, int y);
void drawCircle(int xc, int yc, int r);

imageT *newTexture(imageT *image, vector2 pos, vector2 size);
void moveTexture(imageT *texture, vector2 pos, vector2 size);

void zoom(SDL_Rect *camera);
void dezoom(SDL_Rect *camera);

void verticalTracking(SDL_Rect *camera, int posY);
void horizontalTracking(SDL_Rect *camera, int posX);

textT *newText(TTF_Font *font, char *string, int r, int g, int b, int a);
void displayText(textT *text, vector2 pos);
void refreshText(textT *text, TTF_Font *font, char *string);
void destroyText(textT *text);

#endif //__TOOLBOX_H