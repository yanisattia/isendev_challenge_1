#ifndef __UTILS_H
#define __UTILS_H

#define UNUSED(...) (void)(__VA_ARGS__)

#include <SDL2/SDL_mixer.h>

#include "toolbox.h"
#include "renderer.h"
#include "perlin_noise.h"
#include "namegen.h"
#include "tilemap.h"
#include "chunkgen.h"

/* CONST AND TYPES DEFINITIONS */
#define NBR_GFXASSETS 9

typedef struct engineT engineT;
typedef struct starsT starsT;
typedef struct textT textT;
typedef struct gameDataT gameDataT;

/* GLOBAL VARIABLES */
extern imageT *gfxAssets[NBR_GFXASSETS];
extern engineT engine;
extern int fpsCap;

extern starsT starSystem;
extern imageT *menuPlanet[4];
extern imageT *menuBtn[10];
extern TTF_Font *font[10];
extern textT *gameText[10];
extern textT *fpsText;
extern Mix_Music *music[2];
extern Mix_Chunk *mouseSound;
extern gameDataT *gameData[5];

#endif //__UTILS_H 