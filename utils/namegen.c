#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include "utils.h"

void generateName(string buffer, string syllabesList[], int listSize, int nameSize){
    int randN = 0;
    int lastRand = -1;
    strcpy(buffer, "");

    for(int i = 0; i < nameSize; ++i){
        if(lastRand == -1){
            randN = (float)rand()/(float)RAND_MAX * listSize;
            strcat(buffer, syllabesList[randN]);
            lastRand = randN;
        }else{
            do{
                randN = (float)rand()/(float)RAND_MAX * listSize;
            }while(randN == lastRand);
            strcat(buffer, syllabesList[randN]);
            lastRand = randN;
        }
    }

    //Mise en majuscule (CF Table ASCII)
    buffer[0] -= 32;
}