#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <stdarg.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include "renderer.h"

//renderer specific variables (can be accessed via getters)
static SDL_Window *window = NULL; //this is the window created for the SDL2 context
static SDL_Renderer *renderer = NULL; //this is the renderer created for the SDL2 context
static rendererData windowOptions; //The options to be used in the renderer

static SDL_Rect camera;

void createRenderer(char *windowName){
    //Initializing video module from the SDL2 and checking for errors
    if((SDL_Init(SDL_INIT_VIDEO) < 0)) { 
        printf("FATAL ERROR : Could not initialize SDL: %s\nEXITING NOW\n", SDL_GetError());
        exit(-1);
    }

    //Looking for texture filtering
    if(SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "0")){
        fprintf(stderr, "WARNING : Linear Texture filtering was not enabled\n");
    }   

    SDL_Surface *ico = IMG_Load("icone.ico");

    //Copying windowName in rendererData struct.
    windowOptions.windowName = (char *) malloc(sizeof(char)*(strlen(windowName)+1));
    strcpy(windowOptions.windowName, windowName);

    FILE *windowConf = NULL; //The configuration file
    char fullscreen[255];

        //Trying to open the renderer options file
        windowConf = fopen("config/renderer.ini", "rt");
        if(windowConf){
            //File is correctly opened
            printf("Creating renderer...\n");
            fscanf(windowConf, "windowWidth = %d\nwindowHeight = %d\nfullscreen = %s\nfps_limit = %d\n", 
            &(windowOptions.width), &(windowOptions.height), fullscreen, &(windowOptions.fpsCap));
            //Data has been read from config file and values has been taken to rendererData variables
        }else{
            //Error while opening file
            perror("fopen");
            //fprintf(stderr, "FATAL ERROR : Could not open a configuration file \nEXITING NOW\n");
            exit(1);
        }
        //Closing file properly
        fclose(windowConf);
        windowConf = NULL;
        
        //If the code reaches this point, rendererData is ready to be used
        camera.x = 0;
        camera.y = 0;
        camera.w = 1;
        camera.h = 1;
        //Creating window depending on renderer.ini content
        window = SDL_CreateWindow(
            windowName, 
            SDL_WINDOWPOS_CENTERED, 
            SDL_WINDOWPOS_CENTERED, 
            windowOptions.width, 
            windowOptions.height, 
            SDL_WINDOW_SHOWN
        );
        

        if(strcmp(fullscreen, "true") == 0){
            windowOptions.fullscreen = true;
            SDL_SetWindowFullscreen(window, SDL_WINDOW_FULLSCREEN_DESKTOP);
        }else{
            windowOptions.fullscreen = false;
        }

        //Testing for errors
        if(window == NULL){
            //fprintf(stderr, "FATAL ERROR : SDL_CreateWindow failed : %s\nEXITING NOW\n", SDL_GetError());
            exit(1);
        }

        SDL_SetWindowIcon(window, ico);

        //Creating renderer
        renderer = SDL_CreateRenderer(
            window, 
            -1, 
            SDL_RENDERER_ACCELERATED | SDL_RENDERER_TARGETTEXTURE
        );
        //SDL_RENDERER_PRESENTVSYNC for vsync

        //Testing for errors
        if(renderer == NULL){
            //fprintf(stderr, "FATAL ERROR : SDL_CreateRenderer failed : %s\nEXITING NOW\n", SDL_GetError());
            exit(1);
        }
        printf("Done !\n");
}

void refreshRenderer(void){
    if(windowOptions.fullscreen)
        SDL_SetWindowFullscreen(window, SDL_WINDOW_FULLSCREEN_DESKTOP);
    else
        SDL_SetWindowFullscreen(window, 0);
}

void saveRenderer(void){
    FILE *windowConf = NULL; //The configuration file
    char fullscreen[255];

        if(windowOptions.fullscreen)
            strcpy(fullscreen, "true");
        else
            strcpy(fullscreen, "false");
        //Trying to open the renderer options file
        windowConf = fopen("config/renderer.ini", "wt");
        if(windowConf){
            //File is correctly opened
            printf("Creating renderer...\n");
            fprintf(windowConf, "windowWidth = %d\nwindowHeight = %d\nfullscreen = %s\nfps_limit= %d\n", 
            (windowOptions.width), (windowOptions.height), fullscreen, windowOptions.fpsCap);
            //Data has been read from config file and values has been taken to rendererData variables
        }else{
            //Error while opening file
            perror("fopen");
            //fprintf(stderr, "FATAL ERROR : Could not open a configuration file \nEXITING NOW\n");
            exit(1);
        }
        //Closing file properly
        fclose(windowConf);
        windowConf = NULL;
}

void destroyRenderer(){
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    free(windowOptions.windowName);
    windowOptions.windowName = NULL;
}

SDL_Renderer *getRenderer(){
    return renderer;
}

SDL_Window *getWindow(){
    return window;
}

rendererData *getRendererData(){
    return (&windowOptions);
}

SDL_Rect *getCamera(){
    return (&camera);
}
