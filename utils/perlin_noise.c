#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include "utils.h"

void newPerlinNoise(perlinNoiseT *perlinNoise, int size, int nOctaves){
    //Allocation mémoire pour les tableaux d'entrée (bruit aléatoire)
    //Et de sortie (bruit de perlin)
    perlinNoise->randInput = (float *) malloc(sizeof(float)*size);
    perlinNoise->perlinOutput = (float *) malloc(sizeof(float)*size);

    //Initialisation à 0 de tous les composants
    for(int i = 0; i < size; ++i){
        perlinNoise->randInput[i] = 0;
        perlinNoise->perlinOutput[i] = 0;
    }

    //Réglage des variables de taille et du nombre d'octaves
    perlinNoise->size = size;
    perlinNoise->nOctaves = nOctaves;
}

void generateNoise(perlinNoiseT *perlinNoise){
    for(int i = 0; i < perlinNoise->size; ++i) perlinNoise->randInput[i] = noise();
}

float noise(void){
    float noiseValue = 0.0;
    noiseValue = ((float)rand()/(float)(RAND_MAX)) * 1.0;
    return noiseValue;
}

void perlinNoise1(perlinNoiseT *perlinNoise, int nOctaves){
    perlinNoise->nOctaves = nOctaves;
    for(int i = 0; i < perlinNoise->size; ++i){
        float fNoise = 0.0f;
        float fScale = 1.0f;
        float fScaleAcc = 0.0f;
        for(int j = 0; j < perlinNoise->nOctaves; ++j){
            /*
             * Le bruit de Perlin est en fait l'assemblage de plusieurs octaves de bruits. Chacune des
             * octaves apporte plus ou moins de détail. La première octave étant la moins détaillée
             * (elle ressemble à une ligne droite) et la dernière étant la plus irrégulière.
             * On va donc prendre un échantillon (nPitch) à partir de la taille de notre tableau final
             * que l'on réduit d'une division par 2 en fonction de la valeur de j allant de 0 au nombre d'octaves.
             * De ce fait, le pitch sera de plus en plus petit (l'écart entre les valeurs choisies pour l'approximation)
             * et donc, on aura de plus en plus de détail, à mesure que le nombre d'octaves augmente.
             * Plus d'octaves = plus de variation
             * 
             * Ensuite, on échantillone les valeurs selon le pitch choisi et on accumule le bruit dans la variable fNoise
             * (cf. Plus haut)
            */
            int nPitch = perlinNoise->size >> j;
            if(nPitch == 0) nPitch++;
            int nSample1 = (i / nPitch) * nPitch; //On divise par pitch et on multiplie par pitch (ce sont des int, nSample != i)
            int nSample2 = (nSample1 + nPitch) % perlinNoise->size; //On limite l'échantillon avec notre taille maximale

            float fBlend = (float)(i - nSample1) / (float)nPitch; //Permet de savoir où on se pose par rapport au pitch
            float fSample = (1.0f - fBlend) * perlinNoise->randInput[nSample1] + fBlend * perlinNoise->randInput[nSample2];
            fNoise += fSample*fScale;
            fScaleAcc += fScale;
            fScale = fScale / 2.0f;
        }

        perlinNoise->perlinOutput[i] = fNoise / fScaleAcc; //On attribue la valeur au tableau final
    }
}