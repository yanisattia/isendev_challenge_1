#ifndef __TILEMAP_H
#define __TILEMAP_H

typedef struct tileT tileT;
typedef struct tilemapT tilemapT;

struct tileT{
    vector2 pos;
    imageT *texture;
};

struct tilemapT{
    int mapSize;
    tileT *tiles;
};

void newTileMap(tilemapT *tilemap, int size);
void destroyTileMap(tilemapT *tilemap);

#endif //__TILEMAP_H