#ifndef __RENDERER_H
#define __RENDERER_H

/*
 * @file  : renderer
 * @brief : Creates a renderering environment for SDL2
*/

#define REFRESH_RATE 1

/*
 * @struct : rendererData
 * @brief  : This structure contains data about the rendering environment 
*/
typedef struct rendererData{
    char *windowName;
    int width;
    int height;
    bool fullscreen;
    int fpsCap;
}rendererData;

/*
 * @func   : createRenderer();
 * @params : a string containing the window name to create
 * @return : void
 * @brief  : This functions reads the "renderer.ini" file in order
 * to fill the rendererData structure with enough data to create a
 * complete rendering environment. After this has been done, it creates
 * an SDL2 video context and initialize window and renderer
*/
void createRenderer(char *windowName);

/*
 * @func   : destroyRenderer();
 * @params : void
 * @return : void
 * @brief  : This functions destroys the rendering environment
 * that has been created by "createRenderer()"
*/
void destroyRenderer();

void refreshRenderer(void);
void saveRenderer(void);

/*
 * @func   : getters()
 * @params : void
 * @return : the data to get
 * @brief  : Returns a renderer specific variable (can be used from other .c files)
*/
SDL_Renderer *getRenderer();
SDL_Window *getWindow();
rendererData *getRendererData();
SDL_Rect *getCamera();

#endif // __RENDERER_H