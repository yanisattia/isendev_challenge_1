#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include "../utils/utils.h"
#include "planet.h"
#include "solar_system.h"

void newSolarSystem(solarSystemT *systeme){
    systeme->nb_planetes = (float)rand()/(float)RAND_MAX * MAX_PLANETES + 10;
    if(systeme->nb_planetes > 30)
        systeme->nb_planetes = 30;
    systeme->size = (MAX_RAY + MIN_RAY + (float)rand()/(float)RAND_MAX * MAX_SPACING) * systeme->nb_planetes;
    systeme->planetes = (planetT *) malloc(sizeof(planetT)*systeme->nb_planetes);
    systeme->index = 0;
    
    for(int i = 0; i < systeme->nb_planetes; ++i){
        newPlanet(&(systeme->planetes[i]));
        generatePlanetPos(&(systeme->planetes[i]), i+1);
    }
    string syllabes[10];
    strcpy(syllabes[0], "mon");
    strcpy(syllabes[1], "fay");
    strcpy(syllabes[2], "shi");
    strcpy(syllabes[3], "zag");
    strcpy(syllabes[4], "blar");
    strcpy(syllabes[5], "rash");
    strcpy(syllabes[6], "isen"); //Easter Egg issou
    strcpy(syllabes[7], "kol");
    strcpy(syllabes[8], "car");
    strcpy(syllabes[9], "inh");
    generateName(systeme->name, syllabes, 9, 3);
}

void printSolarSystemInfo(solarSystemT *systeme){
    printf("Système solaire - %s - \n", systeme->name);
    printf("Composé de %d planètes\n", systeme->nb_planetes);
    for(int i = 0; i < systeme->nb_planetes; ++i){
        printPlanetInfo(&(systeme->planetes[i]));
    }
}

void drawSystem(solarSystemT *systeme){
    drawPlanet(&(systeme->planetes[systeme->index]));
}

void saveSolarSystem(solarSystemT *systeme, int nbSave, int id){
    FILE *saveFile;
    string pathPlanet;
    string pathSystem;
    string nbSave_S;
    string id_S;

        sprintf(nbSave_S, "%d", nbSave);
        sprintf(id_S, "%d", id);

        strcpy(pathSystem, "data/saves/slot_");
        strcat(pathSystem, nbSave_S);
        strcat(pathSystem, "/solarsystem_");
        strcat(pathSystem, id_S);
        strcat(pathSystem, ".sav");

        strcpy(pathPlanet, "data/saves/slot_");
        strcat(pathPlanet, nbSave_S);
        strcat(pathPlanet, "/planmap_");
        strcat(pathPlanet, id_S);
        strcat(pathPlanet, ".sav");

        saveFile = fopen(pathPlanet, "wb");
        fwrite(systeme->planetes, sizeof(planetT), systeme->nb_planetes, saveFile);
        fclose(saveFile);

        saveFile = fopen(pathSystem, "wb");
        fwrite(systeme, sizeof(solarSystemT), 1, saveFile);
        fclose(saveFile);
}

void loadSolarSystem(solarSystemT *systeme, int nbSave, int id){
    FILE *saveFile;
    string pathPlanet;
    string pathSystem;
    string nbSave_S;
    string id_S;

        sprintf(nbSave_S, "%d", nbSave);
        sprintf(id_S, "%d", id);

        strcpy(pathSystem, "data/saves/slot_");
        strcat(pathSystem, nbSave_S);
        strcat(pathSystem, "/solarsystem_");
        strcat(pathSystem, id_S);
        strcat(pathSystem, ".sav");

        strcpy(pathPlanet, "data/saves/slot_");
        strcat(pathPlanet, nbSave_S);
        strcat(pathPlanet, "/planmap_");
        strcat(pathPlanet, id_S);
        strcat(pathPlanet, ".sav");

        saveFile = fopen(pathSystem, "rb");
        fread(systeme, sizeof(solarSystemT), 1, saveFile);
        fclose(saveFile);

        saveFile = fopen(pathPlanet, "rb");
        systeme->planetes = malloc(sizeof(planetT)*systeme->nb_planetes);
        fread(systeme->planetes, sizeof(planetT), systeme->nb_planetes, saveFile);
        for(int i = 0; i < systeme->nb_planetes; ++i){
            loadPlaneteTextures(&(systeme->planetes[i]));
        }
        
        fclose(saveFile);
}