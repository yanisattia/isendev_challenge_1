#ifndef __SOLAR_SYSTEM_H
#define __SOLAR_SYSTEM_H

#define MAX_PLANETES 30
#define MAX_SPACING 10 //Le nombre de millier de kilomètres qu'il peut y avoir entre deux planètes

typedef struct solarSystemT solarSystemT;
typedef struct planetT planetT;

struct solarSystemT{
    //WorkInProgress
    string name;

    int size; //en millier de kilomètres
    int nb_planetes;
    planetT *planetes;

    int index;
};

//Fonction qui génère un système solaire de manière procédurale
void newSolarSystem(solarSystemT *systeme);

void printSolarSystemInfo(solarSystemT *systeme);
void drawSystem(solarSystemT *systeme);

void saveSolarSystem(solarSystemT *systeme, int nbSave, int id);
void loadSolarSystem(solarSystemT *systeme, int nbSave, int id);

#endif //__SOLAR_SYSTEM_H