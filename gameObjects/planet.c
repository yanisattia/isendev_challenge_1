#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include "../utils/utils.h"
#include "planet.h"

void newPlanet(planetT *planete){
    string syllabes[13];
    strcpy(syllabes[0], "mon");
    strcpy(syllabes[1], "fay");
    strcpy(syllabes[2], "shi");
    strcpy(syllabes[3], "zag");
    strcpy(syllabes[4], "blar");
    strcpy(syllabes[5], "rach");
    strcpy(syllabes[6], "isen"); //Easter Egg issou
    strcpy(syllabes[7], "kol");
    strcpy(syllabes[8], "car");
    strcpy(syllabes[9], "inh");
    strcpy(syllabes[10], "the");
    strcpy(syllabes[11], "veri");
    strcpy(syllabes[12], "table");
    generateName(planete->name, syllabes, 12, 3);

    planete->ray = 1000*(((float)rand()/(float)RAND_MAX * MAX_RAY) + MIN_RAY);
    planete->toxicity = (float)rand()/(float)RAND_MAX * MAX_TOXICITY;
    planete->type = (float)rand()/(float)RAND_MAX * MAX_TYPE;

    loadPlaneteTextures(planete);
}

void loadPlaneteTextures(planetT* planete){
    if(planete->toxicity >= 0 && planete->toxicity < 33){
        planete->layer1 = newTexture(gfxAssets[5], newVector2(0, 0), newVector2(0, 0));
    }else if(planete->toxicity >= 33 && planete->toxicity < 66){
        planete->layer1 = newTexture(gfxAssets[0], newVector2(0, 0), newVector2(0, 0));
    }else if(planete->toxicity >= 66){
        planete->layer1 = newTexture(gfxAssets[4], newVector2(0, 0), newVector2(0, 0));
    }

    switch(planete->type){
        case 0:
            planete->layer2 = newTexture(gfxAssets[1], newVector2(0, 0), newVector2(0, 0));
        break;

        case 1:
            planete->layer2 = newTexture(gfxAssets[3], newVector2(0, 0), newVector2(0, 0));
        break;

        case 2:
            planete->layer2 = newTexture(gfxAssets[2], newVector2(0, 0), newVector2(0, 0));
        break;

        case 3:
            planete->layer2 = newTexture(gfxAssets[3], newVector2(0, 0), newVector2(0, 0));       
            planete->layer3 = newTexture(gfxAssets[1], newVector2(0, 0), newVector2(0, 0));         
        break;

        case 4:
            planete->layer2 = newTexture(gfxAssets[3], newVector2(0, 0), newVector2(0, 0));
            planete->layer3 = newTexture(gfxAssets[2], newVector2(0, 0), newVector2(0, 0));
        break;

        case 5:
            planete->layer2 = newTexture(gfxAssets[1], newVector2(0, 0), newVector2(0, 0));
            planete->layer3 = newTexture(gfxAssets[2], newVector2(0, 0), newVector2(0, 0));
        break;

        case 6:
            planete->layer2 = newTexture(gfxAssets[1], newVector2(0, 0), newVector2(0, 0));
            planete->layer3 = newTexture(gfxAssets[3], newVector2(0, 0), newVector2(0, 0));
        break;

        case 7:
            planete->layer2 = newTexture(gfxAssets[2], newVector2(0, 0), newVector2(0, 0));
            planete->layer3 = newTexture(gfxAssets[3], newVector2(0, 0), newVector2(0, 0));
        break;

        case 8:
            planete->layer2 = newTexture(gfxAssets[2], newVector2(0, 0), newVector2(0, 0));
            planete->layer3 = newTexture(gfxAssets[1], newVector2(0, 0), newVector2(0, 0));
        break;
    }
}

void generatePlanetPos(planetT *planet, int nb){
    planet->posX = 10000 * MAX_RAY * nb;
    planet->posY = 300 * 1000;
}

void drawPlanet(planetT *planet){
    vector2 winSize;
    SDL_GetWindowSize(getWindow(), &(winSize.x), &(winSize.y));
    moveImage(planet->layer1, newVector2(150, winSize.y/2-planet->ray/600), newVector2(planet->ray/300, planet->ray/300));
    displayImage(planet->layer1, planet->name);
    moveImage(planet->layer2, newVector2(150, winSize.y/2-planet->ray/600), newVector2(planet->ray/300, planet->ray/300));
    displayImage(planet->layer2, planet->name);
    if(planet->type >= 3){
        moveImage(planet->layer3, newVector2(150, winSize.y/2-planet->ray/600), newVector2(planet->ray/300, planet->ray/300));
        displayImage(planet->layer3, planet->name);
    }
    /*
    SDL_Rect rect;
    SDL_Rect *camera = getCamera();
    rect.x = (planet->posX/2000)-(planet->ray/1000);
    rect.y = (planet->posY/2000)-(planet->ray/1000);
    rect.w = planet->ray/500;
    rect.h = planet->ray/500;

    //Testing position in order to draw only what is supposed to be visible on screen by the camera.
    /*if((rect.x+rect.w) > camera->x && (rect.x) < (winSize.x + camera->x) && (rect.y+rect.h) > camera->y && rect.y < (winSize.y + camera->y)){
    }*/
}

void printPlanetInfo(planetT *planete){
    printf("Planete - %s -\n", planete->name);
    printf("> De rayon : %.0f Km\n> Rad : %d \n> Type : %d\n", planete->ray, planete->toxicity, planete->type);
}

void displayPlanetInfo(planetT *planete, TTF_Font *font){
    textT *text;
    char string[255], tmp[10];

    vector2 winSize, pos;
    SDL_GetWindowSize(getWindow(), &(winSize.x), &(winSize.y));
    
    pos.x = planete->ray/300 + 150; pos.y = winSize.y/2-planete->ray/600;

    strcpy(string, "Planete : ");
    strcat(string, planete->name);
    text = newText(font, string, 255, 255, 255, 255);
    displayText(text, pos);
    SDL_FreeSurface(text->textSurface);
    SDL_DestroyTexture(text->textTexture);
    free(text);

    strcpy(string, "Rayon : ");
    sprintf(tmp, "%.2f", planete->ray);
    strcat(string, tmp);
    strcat(string, " km");
    text = newText(font, string, 255, 255, 255, 255);
    pos.y +=  20;
    displayText(text, pos);
    SDL_FreeSurface(text->textSurface);
    SDL_DestroyTexture(text->textTexture);
    free(text);

    strcpy(string, "Rad : ");
    sprintf(tmp, "%d", planete->toxicity);
    strcat(string, tmp);
    strcat(string, " %");
    text = newText(font, string, 255, 255, 255, 255);
    pos.y +=  20;
    displayText(text, pos);
    SDL_FreeSurface(text->textSurface);
    SDL_DestroyTexture(text->textTexture);
    free(text);
}