#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include "../utils/utils.h"
#include "ui.h"

void newCanvas(uiCanvasT *canvas, int nb_elem, char *name){
    canvas->active = 1;
    canvas->visible = 1;

    canvas->name = (char *)malloc(sizeof(char)*strlen(name));
    strcpy(canvas->name, name);
    canvas->nb_elements = nb_elem;

    canvas->elements = (uiElementT *)malloc(sizeof(uiElementT)*nb_elem);
    for(int i = 0; i < nb_elem; ++i){
        canvas->elements[i].active = 1;
        canvas->elements[i].hover = 0;
        canvas->elements[i].state = 0;
        canvas->elements[i].visible = 1;
        canvas->elements[i].image = NULL;
        canvas->elements[i].text = NULL;
    }
}

void newElement(uiCanvasT *canvas, int id, textT *text, imageT *image, vector2 pos){
    //newText and newTexture should work fine as params.
    if(id >= 0 && id < canvas->nb_elements){
        canvas->elements[id].image = image;
        canvas->elements[id].text = text;
        canvas->elements[id].text->pos.x = pos.x;
        canvas->elements[id].text->pos.y = pos.y;
    }else{
        printf("# FATAL ERROR : Cannot access this id (%d) in canvas %s\n", id, canvas->name);
    }
}

void displayCanvas(uiCanvasT *canvas){
    if(canvas->visible){
        for(int i = 0; i < canvas->nb_elements; ++i){
            if(canvas->elements[i].visible){
                if(canvas->elements[i].image != NULL){
                    displayImage(canvas->elements[i].image, "canvas element");
                }
                if(canvas->elements[i].text != NULL)
                    displayText(canvas->elements[i].text, canvas->elements[i].text->pos);
            }
        }
    }
}

void checkHoverCanvas(uiCanvasT *canvas){
    if(canvas->active && canvas->visible){
        for(int i = 0; i < canvas->nb_elements; ++i){
            if(canvas->elements[i].active && canvas->elements[i].visible){
                int mouseX, mouseY;
                SDL_GetMouseState(&mouseX, &mouseY);
                if(mouseX > canvas->elements[i].image->pos.x && mouseX < canvas->elements[i].image->pos.x+canvas->elements[i].image->size.x && mouseY > canvas->elements[i].image->pos.y && mouseY < canvas->elements[i].image->pos.y+canvas->elements[i].image->size.y){
                    if(canvas->elements[i].hover == 0)
                        canvas->elements[i].hover = 1;
                }else{
                    canvas->elements[i].hover = 0;
                }
            }
        }
    }
}

void checkClicCanvas(uiCanvasT *canvas){
    if(canvas->active && canvas->visible){
        for(int i = 0; i < canvas->nb_elements; ++i){
            if(canvas->elements[i].active && canvas->elements[i].visible){
                if(canvas->elements[i].hover > 0)
                    ++canvas->elements[i].state;
            }
        }
    }
}

void destroyCanvas(uiCanvasT *canvas){
    for(int i = 0; i < canvas->nb_elements; ++i){
        destroyImage(canvas->elements[i].image);
        destroyText(canvas->elements[i].text);
    }
    free(canvas->name);
    free(canvas->elements);
    canvas->elements = NULL;
}