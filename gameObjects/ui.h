#ifndef __UI_H
#define __UI_H

typedef struct uiCanvasT uiCanvasT;
typedef struct uiElementT uiElementT;

struct uiCanvasT{
    int active;
    int visible;

    int nb_elements;
    char *name;

    uiElementT *elements;
};

struct uiElementT{
    int hover;
    int active;
    int visible;
    int state;

    imageT *image;
    textT *text;
};

void newCanvas(uiCanvasT *canvas, int nb_elem, char *name);
void newElement(uiCanvasT *canvas, int id, textT *text, imageT *image, vector2 pos);
void displayCanvas(uiCanvasT *canvas);
void checkHoverCanvas(uiCanvasT *canvas);
void checkClicCanvas(uiCanvasT *canvas);
void destroyCanvas(uiCanvasT *canvas);

#endif //__UI_H