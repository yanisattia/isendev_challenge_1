#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include "../utils/utils.h"
#include "stars.h"

void generateStars(starsT *starSystem, int nb_stars){
    starSystem->nb_stars = nb_stars;
    newPerlinNoise(&(starSystem->perlinNoise), nb_stars, 1);
    generateNoise(&(starSystem->perlinNoise));
}

void displayStars(starsT *starSystem){
    int winW, winH;
    SDL_GetWindowSize(getWindow(), &winW, &winH);

    int step = winW/starSystem->nb_stars;

    SDL_SetRenderDrawColor(getRenderer(), 255, 255, 255, 0);

    for(int i = 0; i < starSystem->nb_stars; ++i){
        drawPixel((i+1)*step, starSystem->perlinNoise.randInput[i]*winH);
    }
}

void animateStars(starsT *starSystem, int offsetX, int offsetY){
    int winW, winH;
    SDL_GetWindowSize(getWindow(), &winW, &winH);

    winW *= 2;
    winH *= 2;

    int step = winW/starSystem->nb_stars;

    SDL_SetRenderDrawColor(getRenderer(), 255, 255, 255, 0);

    for(int i = 0; i < starSystem->nb_stars; ++i){
        drawPixel((i+1)*step + offsetX - 100, starSystem->perlinNoise.randInput[i]*winH + offsetY - 100);
    }
}