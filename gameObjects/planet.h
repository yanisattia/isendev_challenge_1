#ifndef __PLANET_H
#define __PLANET_H

#define MAX_TYPE 9 //Valeur maximale du type

#define MAX_TOXICITY 100 //Toxicité mesurée en pourcentages
#define MAX_RAY 150 //Rayon mesuré en millers de kilomètres
#define MIN_RAY 50 //Rayon minimum

typedef struct planetT planetT;

struct planetT{
    //WorkInProgress
    string name; //Namegen
    int type; //Le type permettra de définir le modèle de planète utilisé
    //int space_type; //Permet de définir s'il doit y avoir des astéroides/couronnes dans l'espace voisin

    int posX, posY; //Position du centre de la planète dans le système solaire

    int toxicity; //Valeur de toxicité (définira aussi les couches visuelles)
    float ray; //Rayon de la planète
    
    //Visuals
    imageT *layer1; 
    imageT *layer2; 
    imageT *layer3; 

};

//Fonction qui génère une planète de manière procédurale
void newPlanet(planetT *planet);

//Charge en mémoire les textures d'une planète
void loadPlaneteTextures(planetT* planete);

//Fonction qui génère la position d'une planète
void generatePlanetPos(planetT *planet, int nb);

//Fonction qui affiche une planète
void drawPlanet(planetT *planet);

//Fonction qui affiche les informations d'une planète
void printPlanetInfo(planetT *planete);

void displayPlanetInfo(planetT *planete, TTF_Font *font);

#endif //__PLANET_H