#ifndef __STARS_H
#define __STARS_H

typedef struct starsT starsT;
typedef struct perlinNoiseT perlinNoiseT;

struct starsT{
    int nb_stars;
    perlinNoiseT perlinNoise;
};

void generateStars(starsT *starSystem, int nb_stars);
void displayStars(starsT *starSystem);
void animateStars(starsT *starSystem, int offsetX, int offsetY);

#endif //__STARS_H