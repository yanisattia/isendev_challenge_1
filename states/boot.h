#ifndef __BOOT_H
#define __BOOT_H

//The main boot function
void bootUpdate(void);

//The drawing that must be done while booting
void bootDraw(void);

#endif //__BOOT_H


