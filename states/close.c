#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>

#include "../utils/utils.h"
#include "states.h"

#include "../gameObjects/ui.h"

void closeUpdate(void){
    loadingScreen();
    engine.process = false;
    printf("\n");
}

void closeDraw(void){
    
}