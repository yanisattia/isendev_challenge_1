#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_mixer.h>

#include "../utils/utils.h"
#include "../gameObjects/stars.h"
#include "../gameObjects/ui.h"
#include "states.h"

/*
 * Le fichier menu.c contient les fonctions de dessin et d'actualisation
 * du programme dans son état "menu principal". Ici, on affiche et on gère
 * les différents boutons auxquels l'utilisateur aura accès pour lancer
 * la partie, accéder aux options, etc... Pour passer au jeu, il faudra 
 * appeler "setState(GAME)"
*/
int winW, winH;
float starOffX = 0.0, starOffY = 0.0;
float starOffStepX = 0.2, starOffStepY = 0.3;

void menuUpdate(void){
    SDL_GetWindowSize(getWindow(), &winW, &winH);

    while(SDL_PollEvent(&(engine.event))){
        switch(engine.event.type){
            //If a key up event is detected
            case SDL_KEYUP:
                switch(engine.event.key.keysym.sym){
                    case SDLK_ESCAPE:
                        //Ends the program
                        setState(CLOSE);
                    break;

                    case SDLK_c:
                        fpsCap*=-1;
                    break;

                    case SDLK_f:
                        engine.displayFps*=-1;
                    break;
                }
            break;

            case SDL_KEYDOWN:
                switch(engine.event.key.keysym.sym){
                }
            break;

            case SDL_MOUSEBUTTONDOWN:
                switch (engine.event.button.button){
                    case SDL_BUTTON_LEFT:
                        checkClicCanvas(&(engine.ui));
                        checkClicCanvas(&(engine.new_game));
                        checkClicCanvas(&(engine.load_game));
                        checkClicCanvas(&(engine.options_menu));
                    break;

                    case SDL_BUTTON_RIGHT:
                        
                    break;

                    default:
                        
                    break;
                }
            break;
        }
    }

    if(engine.ui.elements[3].state > 0){
        Mix_PlayChannel(-1, mouseSound, 0);
        setState(CLOSE);
    }

    //New game
    if(engine.ui.elements[0].state > 0){
        Mix_PlayChannel(-1, mouseSound, 0);
        if(engine.new_game.active == 0){
            engine.new_game.active = 1;
            engine.new_game.visible = 1;
        }else if(engine.new_game.active == 1){
            engine.new_game.active = 0;
            engine.new_game.visible = 0;
        }
        engine.load_game.active = 0;
        engine.load_game.visible = 0;
        engine.options_menu.active = 0;
        engine.options_menu.visible = 0;
        engine.ui.elements[0].state = 0;
    }

    //Load game
    if(engine.ui.elements[1].state > 0){
        Mix_PlayChannel(-1, mouseSound, 0);
        if(engine.load_game.active == 0){
            engine.load_game.active = 1;
            engine.load_game.visible = 1;
        }else if(engine.load_game.active == 1){
            engine.load_game.active = 0;
            engine.load_game.visible = 0;
        }
        engine.new_game.active = 0;
        engine.new_game.visible = 0;
        engine.options_menu.active = 0;
        engine.options_menu.visible = 0;
        engine.ui.elements[1].state = 0;
    }

    //Options
    if(engine.ui.elements[2].state > 0){
        Mix_PlayChannel(-1, mouseSound, 0);
        if(engine.options_menu.active == 0){
            engine.options_menu.active = 1;
            engine.options_menu.visible = 1;
        }else if(engine.options_menu.active == 1){
            engine.options_menu.active = 0;
            engine.options_menu.visible = 0;
        }
        engine.new_game.active = 0;
        engine.new_game.visible = 0;
        engine.load_game.active = 0;
        engine.load_game.visible = 0;
        engine.ui.elements[2].state = 0;
    }

    if(engine.options_menu.elements[1].state > 0){
        Mix_PlayChannel(-1, mouseSound, 0);
        engine.render->fullscreen = !engine.render->fullscreen;
        refreshRenderer();
        saveRenderer();
        engine.options_menu.elements[1].state = 0;
        setState(BOOT);
    }

    for(int i = 1; i < engine.new_game.nb_elements; ++i){
        if(engine.new_game.elements[i].state > 0){
            Mix_PlayChannel(-1, mouseSound, 0);
            engine.loading = 1;
            loadingScreen(0);
            addSolarSystem(gameData[i-1]);
            loadingScreen(20);
            engine.loaded = i-1;
            loadingScreen(40);
            clearGameData(gameData[i-1]);
            loadingScreen(60);
            gameData[i-1] = loadGameData(i-1);
            loadingScreen(80);
            loadSystems(gameData[i-1]);
            loadingScreen(100);
            setState(GAME);
            engine.loading = 0;
            engine.new_game.elements[i].state = 0;
            engine.new_game.active = 0;
            engine.new_game.visible = 0;
        }
    }

    for(int i = 1; i < engine.load_game.nb_elements; ++i){
        if(i <= 5){
            if(engine.load_game.elements[i].state > 0){
                Mix_PlayChannel(-1, mouseSound, 0);
                engine.loading = 1;
                loadingScreen(0);
                loadSystems(gameData[i-1]);
                loadingScreen(50);
                engine.loaded = i-1;
                loadingScreen(100);
                setState(GAME);
                engine.loading = 0;
                engine.load_game.elements[i].state = 0;
                engine.load_game.active = 0;
                engine.load_game.visible = 0;
            }
        }else{
            if(engine.load_game.elements[i].state > 0){
                Mix_PlayChannel(-1, mouseSound, 0);
                engine.loading = 1;
                loadingScreen(0);
                resetSaves(i-6);
                loadingScreen(50);
                gameData[i-6] = loadGameData(i-6);
                loadingScreen(100);
                engine.loading = 0;
                engine.load_game.elements[i].state = 0;
            }
        }
    }

    for(int i = 0; i < 5; ++i){
        if(gameData[i]->empty){
            engine.load_game.elements[i+1].active = 0;
            engine.new_game.elements[i+1].image->alpha = 127;
            engine.new_game.elements[i+1].text->color.a = 255;
            engine.load_game.elements[i+1].image->alpha = 50;
            engine.load_game.elements[i+1].text->color.a = 50;
            engine.new_game.elements[i+1].active = 1;
        }else{
            engine.load_game.elements[i+1].active = 1;
            engine.new_game.elements[i+1].image->alpha = 50;
            engine.new_game.elements[i+1].text->color.a = 50;
            engine.load_game.elements[i+1].image->alpha = 127;
            engine.load_game.elements[i+1].text->color.a = 255;
            engine.new_game.elements[i+1].active = 0;
        }
    }
}

void menuDraw(void){
    animateStars(&(starSystem), starOffX, starOffY);
    if(((int)engine.frameCounter%300) == 0){
        if(starOffStepY > 0)
            starOffStepY = (0.1 + ((float)rand()/(float)(RAND_MAX))) * -0.3;
        else
            starOffStepY = (0.1 + ((float)rand()/(float)(RAND_MAX))) * 0.3;
    }
    if(((int)engine.frameCounter%700) == 0){
        if(starOffStepX > 0)
            starOffStepX = (0.1 + ((float)rand()/(float)(RAND_MAX))) * -0.3;
        else
            starOffStepX = (0.1 + ((float)rand()/(float)(RAND_MAX))) * 0.3;
    }
    starOffX += (float)starOffStepX;
    starOffY += (float)starOffStepY;
    for(int i = 0; i < 4; ++i){
        displayImage((menuPlanet[i]), "menu planet");
    }

    checkHoverCanvas(&(engine.ui));
    checkHoverCanvas(&(engine.new_game));
    checkHoverCanvas(&(engine.load_game));
    checkHoverCanvas(&(engine.options_menu));
    for(int i = 0; i < engine.ui.nb_elements; ++i){
        if(engine.ui.elements[i].active){
            if(engine.ui.elements[i].hover > 0){
                if(engine.ui.elements[i].hover == 1){
                    Mix_PlayChannel(-1, mouseSound, 0);
                    ++engine.ui.elements[i].hover;
                }
                engine.ui.elements[i].image->alpha = 255;
            }else{
                engine.ui.elements[i].image->alpha = 127;
            }
        }
    }
    for(int i = 0; i < engine.new_game.nb_elements; ++i){
        if(engine.new_game.elements[i].active){
            if(engine.new_game.elements[i].hover > 0){
                if(engine.new_game.elements[i].hover == 1){
                    if(i >= 1)
                        Mix_PlayChannel(-1, mouseSound, 0);
                    ++engine.new_game.elements[i].hover;
                }
                engine.new_game.elements[i].image->alpha = 255;
            }else{
                engine.new_game.elements[i].image->alpha = 127;
            }
        }
    }
    for(int i = 0; i < engine.load_game.nb_elements; ++i){
        if(engine.load_game.elements[i].active){
            if(engine.load_game.elements[i].hover > 0){
                if(engine.load_game.elements[i].hover == 1){
                    if(i >= 1)
                        Mix_PlayChannel(-1, mouseSound, 0);
                    ++engine.load_game.elements[i].hover;
                }
                engine.load_game.elements[i].image->alpha = 255;
            }else{
                engine.load_game.elements[i].image->alpha = 127;
            }
        }
    }
    for(int i = 0; i < engine.options_menu.nb_elements; ++i){
        if(engine.options_menu.elements[i].active){
            if(engine.options_menu.elements[i].hover > 0){
                if(engine.options_menu.elements[i].hover == 1){
                    if(i >= 1)
                        Mix_PlayChannel(-1, mouseSound, 0);
                    ++engine.options_menu.elements[i].hover;
                }
                engine.options_menu.elements[i].image->alpha = 255;
            }else{
                engine.options_menu.elements[i].image->alpha = 127;
            }
        }
    }
    displayCanvas(&(engine.ui));
    displayCanvas(&(engine.new_game));
    displayCanvas(&(engine.load_game));
    displayCanvas(&(engine.options_menu));
    //Display title
    displayText((gameText[0]), newVector2((winW-gameText[0]->size.x)/2, 15));
    displayText(gameText[5], newVector2(0, winH-20));
    if(engine.displayFps == 1)
        displayText(fpsText, newVector2(0, 0));
}