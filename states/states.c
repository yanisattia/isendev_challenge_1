#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>

#include "../utils/utils.h"
#include "states.h"
#include "../gameObjects/ui.h"

static int state = 0;

int getState(){
    return state;
}

void setState(int x){
    state = x;
}