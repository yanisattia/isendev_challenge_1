#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_mixer.h>

#include "../utils/utils.h"
#include "../gameObjects/stars.h"
#include "../gameObjects/ui.h"
#include "states.h"

/*
 * Le fichier boot.c contient les fonctions d'actualisation et de dessin 
 * qui sont appelées au démarrage du programme. la fonction bootUpdate() 
 * doit donc se terminer par un "setState(MENU)" pour indiquer au programme
 * que le boot est fini, et donc qu'il doit passer au menu principal.
*/

//Creating images and textures
starsT starSystem;
imageT *menuPlanet[4] = {NULL};

//HMI ASSETS
imageT *menuBtn[10] = {NULL};

void bootUpdate(void){
    loadingScreen(0);
    /* load game saves */
    for(int i = 0; i < 5; ++i){
        if(gameData[i] == NULL)
            gameData[i] = loadGameData(i);
    }
    loadingScreen(33);
    /* Initialising gfxAssets */
    printf("# Now Loading textures and assets\n");
    if(gfxAssets[0] != NULL){
        for(int i = 0; i < 9; ++i){
            destroyImage(gfxAssets[i]);
            gfxAssets[i] = NULL;
        }
    }
    gfxAssets[0] = newImage("data/gameAssets/planet.png");
    gfxAssets[1] = newImage("data/gameAssets/planet_blue.png");
    gfxAssets[2] = newImage("data/gameAssets/planet_green.png");
    gfxAssets[3] = newImage("data/gameAssets/planet_orange.png");
    gfxAssets[4] = newImage("data/gameAssets/planet_red.png");
    gfxAssets[5] = newImage("data/gameAssets/planet_blanc.png");
    gfxAssets[6] = newImage("data/gameAssets/textures/red_grass.png");
    gfxAssets[7] = newImage("data/assets/menu_btn.png");
    gfxAssets[8] = newImage("data/assets/bg_menu.png");
    int winW, winH, rayon;
    int randomVal = 0;
    SDL_GetWindowSize(getWindow(), &winW, &winH);
    if(winH >= winW){
        rayon = winH/2;
    }else{
        rayon = winW/2;
    }
    if(menuPlanet[0] != NULL){
        for(int i = 0; i < 4; ++i){
            destroyImage(menuPlanet[i]);
            menuPlanet[i] = NULL;
        }
    }
    int layer0[3] = {0, 4, 5};
    randomVal = ((float)rand()/(float)(RAND_MAX)) * 3.0;
    menuPlanet[0] = newTexture((gfxAssets[layer0[randomVal]]), newVector2(0, 0), newVector2(0, 0));
    moveImage((menuPlanet[0]), newVector2(winW-(rayon/2), winH/2-(rayon/2)), newVector2(rayon, rayon));
    for(int i = 1; i < 4; ++i){
        randomVal = 1.0+((float)rand()/(float)(RAND_MAX)) * 3.0;
        menuPlanet[i] = newTexture((gfxAssets[randomVal]), newVector2(0, 0), newVector2(0, 0));
        moveImage((menuPlanet[i]), newVector2(winW-(rayon/2), winH/2-(rayon/2)), newVector2(rayon, rayon));
    }

    if(menuBtn[0] != NULL){
        for(int i = 0; i < 7; ++i){
            destroyImage(menuBtn[i]);
            menuBtn[i] = NULL;
        }
    }
    menuBtn[0] = newTexture((gfxAssets[7]), newVector2(winW-winW/3-100, winH/2-winH/4+100), newVector2(0, 0));
    menuBtn[1] = newTexture((gfxAssets[7]), newVector2(winW-winW/3-66, winH/2-winH/4+200), newVector2(0, 0));
    menuBtn[2] = newTexture((gfxAssets[7]), newVector2(winW-winW/3-33, winH/2-winH/4+300), newVector2(0, 0));
    menuBtn[3] = newTexture((gfxAssets[7]), newVector2(winW-winW/3, winH/2-winH/4+400), newVector2(0, 0));
    menuBtn[4] = newTexture((gfxAssets[8]), newVector2((winW/2+20)-gfxAssets[8]->size.x, (winH/2)-gfxAssets[8]->size.y/2 + 20), newVector2(0, 0));
    menuBtn[5] = newTexture((gfxAssets[8]), newVector2((winW/2+20)-gfxAssets[8]->size.x, (winH/2)-gfxAssets[8]->size.y/2 + 20), newVector2(0, 0));
    menuBtn[6] = newTexture((gfxAssets[8]), newVector2((winW/2+20)-gfxAssets[8]->size.x, (winH/2)-gfxAssets[8]->size.y/2 + 20), newVector2(0, 0));

    if(gameText[0] != NULL){
        for(int i = 0; i < 7; ++i){
            destroyText(gameText[i]);
            gameText[i] = NULL;
        }
    }
    gameText[0] = newText(font[5], "Odyssey", 255, 255, 255, 255);
    gameText[1] = newText(font[1], "Nouvelle partie", 255, 255, 255, 255);
    gameText[2] = newText(font[1], "Continuer", 255, 255, 255, 255);
    gameText[3] = newText(font[1], "Options", 255, 255, 255, 255);
    gameText[4] = newText(font[1], "Quitter", 255, 255, 255, 255);
    gameText[5] = newText(font[1], "Odyssey - Alpha version - 0.1.0", 255, 255, 255, 255);
    string tmp;
    if(engine.render->fullscreen)
        strcpy(tmp, "Plein ecran");
    else
        strcpy(tmp, "Fenetre");
    gameText[6] = newText(font[1], tmp, 255, 255, 255, 255);
    if(engine.ui.elements != NULL){
        /*destroyCanvas(&(engine.ui));
        destroyCanvas(&(engine.new_game));
        destroyCanvas(&(engine.load_game));
        destroyCanvas(&(engine.options_menu));*/
    }
    newCanvas(&(engine.ui), 4, "main_ui");
    newCanvas(&(engine.new_game), 6, "new_game");
    newCanvas(&(engine.load_game), 11, "load_game");
    newCanvas(&(engine.options_menu), 2, "options_menu");
    
    loadingScreen(66);
    /* MAIN MENU CANVAS */
    //New game
    newElement(&(engine.ui), 0, gameText[1], menuBtn[0], newVector2(winW-winW/3-75, winH/2-winH/4+115));
    int tmpX, tmpY;
    tmpX = (winW/2+20)-gfxAssets[8]->size.x;
    tmpY = (winH/2)-gfxAssets[8]->size.y/2 + 20;
    newElement(&(engine.new_game), 0, newText(font[1], "Nouvelle partie", 255, 255, 255, 255), menuBtn[4], newVector2(tmpX+50, tmpY+20));


    newElement(
        &(engine.new_game), 1, 
        newText(font[1], "Emplacement I", 255, 255, 255, 255), 
        newTexture(gfxAssets[7], newVector2(tmpX+50, tmpY+70), newVector2(0, 0)), newVector2(tmpX+70, tmpY+90)
    );
    newElement(
        &(engine.new_game), 2, 
        newText(font[1], "Emplacement II", 255, 255, 255, 255), 
        newTexture(gfxAssets[7], newVector2(tmpX+50, tmpY+170), newVector2(0, 0)), newVector2(tmpX+70, tmpY+190)
    );
    newElement(
        &(engine.new_game), 3, 
        newText(font[1], "Emplacement III", 255, 255, 255, 255), 
        newTexture(gfxAssets[7], newVector2(tmpX+50, tmpY+270), newVector2(0, 0)), newVector2(tmpX+70, tmpY+290)
    );
    newElement(
        &(engine.new_game), 4, 
        newText(font[1], "Emplacement IV", 255, 255, 255, 255), 
        newTexture(gfxAssets[7], newVector2(tmpX+50, tmpY+370), newVector2(0, 0)), newVector2(tmpX+70, tmpY+390)
    );
    newElement(
        &(engine.new_game), 5, 
        newText(font[1], "Emplacement V", 255, 255, 255, 255), 
        newTexture(gfxAssets[7], newVector2(tmpX+50, tmpY+470), newVector2(0, 0)), newVector2(tmpX+70, tmpY+490)
    );
    engine.new_game.active = 0;
    engine.new_game.visible = 0;
    //Continue
    newElement(&(engine.ui), 1, gameText[2], menuBtn[1], newVector2(winW-winW/3-41, winH/2-winH/4+215));
    newElement(&(engine.load_game), 0, newText(font[1], "Continuer", 255, 255, 255, 255), menuBtn[5], newVector2(tmpX+50, tmpY+20));
    newElement(
        &(engine.load_game), 1, 
        newText(font[1], "Emplacement I", 255, 255, 255, 255), 
        newTexture(gfxAssets[7], newVector2(tmpX+50, tmpY+70), newVector2(0, 0)), newVector2(tmpX+70, tmpY+90)
    );
    newElement(
        &(engine.load_game), 6, 
        newText(font[1], "X", 255, 255, 255, 255),
        newTexture(gfxAssets[7], newVector2(tmpX+380, tmpY+70), newVector2(0, 0)), newVector2(tmpX+400, tmpY+70)
    );
    newElement(
        &(engine.load_game), 2, 
        newText(font[1], "Emplacement II", 255, 255, 255, 255), 
        newTexture(gfxAssets[7], newVector2(tmpX+50, tmpY+170), newVector2(0, 0)), newVector2(tmpX+70, tmpY+190)
    );
    newElement(
        &(engine.load_game), 7, 
        newText(font[1], "X", 255, 255, 255, 255),
        newTexture(gfxAssets[7], newVector2(tmpX+380, tmpY+170), newVector2(0, 0)), newVector2(tmpX+400, tmpY+170)
    );
    newElement(
        &(engine.load_game), 3, 
        newText(font[1], "Emplacement III", 255, 255, 255, 255), 
        newTexture(gfxAssets[7], newVector2(tmpX+50, tmpY+270), newVector2(0, 0)), newVector2(tmpX+70, tmpY+290)
    );
    newElement(
        &(engine.load_game), 8, 
        newText(font[1], "X", 255, 255, 255, 255),
        newTexture(gfxAssets[7], newVector2(tmpX+380, tmpY+270), newVector2(0, 0)), newVector2(tmpX+400, tmpY+270)
    );
    newElement(
        &(engine.load_game), 4, 
        newText(font[1], "Emplacement IV", 255, 255, 255, 255), 
        newTexture(gfxAssets[7], newVector2(tmpX+50, tmpY+370), newVector2(0, 0)), newVector2(tmpX+70, tmpY+390)
    );
    newElement(
        &(engine.load_game), 9, 
        newText(font[1], "X", 255, 255, 255, 255),
        newTexture(gfxAssets[7], newVector2(tmpX+380, tmpY+370), newVector2(0, 0)), newVector2(tmpX+400, tmpY+370)
    );
    newElement(
        &(engine.load_game), 5, 
        newText(font[1], "Emplacement V", 255, 255, 255, 255), 
        newTexture(gfxAssets[7], newVector2(tmpX+50, tmpY+470), newVector2(0, 0)), newVector2(tmpX+70, tmpY+490)
    );
    newElement(
        &(engine.load_game), 10, 
        newText(font[1], "X", 255, 255, 255, 255),
        newTexture(gfxAssets[7], newVector2(tmpX+380, tmpY+470), newVector2(0, 0)), newVector2(tmpX+400, tmpY+470)
    );
    engine.load_game.active = 0;
    engine.load_game.visible = 0;
    
    //Options
    newElement(&(engine.ui), 2, gameText[3], menuBtn[2], newVector2(winW-winW/3-8, winH/2-winH/4+315));
    newElement(&(engine.options_menu), 0, newText(font[1], "Options", 255, 255, 255, 255), menuBtn[6], newVector2(tmpX+50, tmpY+20));
    newElement(&(engine.options_menu), 1, gameText[6], newTexture(gfxAssets[7], newVector2(tmpX+50, tmpY+70), newVector2(0, 0)), newVector2(tmpX+70, tmpY+90));
    engine.options_menu.active = 0;
    engine.options_menu.visible = 0;

    //Quit
    newElement(&(engine.ui), 3, gameText[4], menuBtn[3], newVector2(winW-winW/3+25, winH/2-winH/4+415));
    for(int i = 0; i < engine.ui.nb_elements; ++i){
        engine.ui.elements[i].image->alpha = 127;
    }
    for(int i = 0; i < engine.new_game.nb_elements; ++i){
        engine.new_game.elements[i].image->alpha = 127;
    }
    for(int i = 0; i < engine.load_game.nb_elements; ++i){
        engine.load_game.elements[i].image->alpha = 127;
    }
    loadingScreen(100);

    generateStars(&starSystem, 200);

    if(Mix_PlayingMusic() == 0)
        Mix_PlayMusic(music[0], -1);
    setState(MENU);
    //printf("Boot state passed\n");
}

void bootDraw(void){
}