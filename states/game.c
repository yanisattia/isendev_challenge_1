#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>

#include "../utils/utils.h"
#include "states.h"

#include "../gameObjects/planet.h"
#include "../gameObjects/solar_system.h"
#include "../gameObjects/ui.h"

/*
 * C'est ici que les fonctions principales concernant
 * la partie de jeu sont appellées
*/
SDL_Rect *camera;
vector2 winSize;

void gameUpdate(void){
    camera = getCamera();
    SDL_GetWindowSize(getWindow(), &(winSize.x), &(winSize.y));
    while(SDL_PollEvent(&(engine.event))){
        switch(engine.event.type){
            //If a key up event is detected
            case SDL_KEYUP:
                switch(engine.event.key.keysym.sym){
                    case SDLK_ESCAPE:
                        //Ends the program
                        engine.loading = 1;
                        loadingScreen();
                        saveGameData(gameData[engine.loaded]);
                        clearGameData(gameData[engine.loaded]);
                        gameData[engine.loaded] = loadGameData(engine.loaded);
                        camera->x = 0;
                        camera->y = 0;
                        dezoom(camera);
                        setState(MENU);
                        engine.loading = 0;
                    break;

                    case SDLK_c:
                        fpsCap*=-1;
                    break;

                    case SDLK_RETURN:
                        
                    break;

                    case SDLK_SPACE:
                    break;

                    case SDLK_KP_PLUS:
                        zoom(camera);
                    break;

                    case SDLK_KP_MINUS:
                        dezoom(camera);
                    break;
                }
            break;

            case SDL_KEYDOWN:
                switch(engine.event.key.keysym.sym){
                    case SDLK_LEFT:
                    case SDLK_q:
                        if(gameData[engine.loaded]->systems[0].index > 0)
                            --gameData[engine.loaded]->systems[0].index;
                        else
                            gameData[engine.loaded]->systems[0].index = gameData[engine.loaded]->systems[0].nb_planetes-1;
                    break;

                    case SDLK_RIGHT:
                    case SDLK_d:
                        if(gameData[engine.loaded]->systems[0].index < gameData[engine.loaded]->systems[0].nb_planetes-1)
                            ++gameData[engine.loaded]->systems[0].index;
                        else
                            gameData[engine.loaded]->systems[0].index = 0;
                    break;
                }
            break;
        }
    }

    /* STUFF TO DO EVERY LOOP */
}

void gameDraw(void){
    /* GAME DRAWING LOOP */
    drawSystem(&(gameData[engine.loaded]->systems[0]));
    displayPlanetInfo(&(gameData[engine.loaded]->systems[0].planetes[gameData[engine.loaded]->systems[0].index]), font[2]);
    if(engine.displayFps == 1)
        displayText(fpsText, newVector2(0, 0));
}