#include "boot.h"
#include "menu.h"
#include "game.h"
#include "close.h"
#include "../engine.h"
#include "../gameObjects/planet.h"
#include "../gameObjects/solar_system.h"

enum statesE {BOOT, MENU, GAME, CLOSE};

int getState(void);
void setState(int x);
