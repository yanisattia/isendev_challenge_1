CFLAGS = -Wall -Wextra -g -std=c99

CLIBS = -lSDL2 -lSDL2_image -lSDL2_ttf -lSDL2_mixer -lm 

WLIBS = -fno-stack-protector -lssp -lmsvcrt -mwindows -lmingw32 -lSDL2main 

# Edition des liens #
linux: engine.o utils/toolbox.o utils/renderer.o utils/perlin_noise.o utils/namegen.o utils/chunkgen.o utils/tilemap.o states/states.o states/boot.o states/menu.o states/game.o states/close.o gameObjects/planet.o gameObjects/solar_system.o gameObjects/stars.o gameObjects/ui.o
	gcc engine.o utils/toolbox.o utils/renderer.o utils/perlin_noise.o utils/namegen.o utils/tilemap.o utils/chunkgen.o states/states.o states/boot.o states/menu.o states/game.o states/close.o gameObjects/planet.o gameObjects/solar_system.o gameObjects/stars.o gameObjects/ui.o -o Odyssey $(CLIBS)

windows: engineW.o utils/toolboxW.o utils/rendererW.o utils/perlin_noiseW.o utils/namegenW.o utils/chunkgenW.o utils/tilemapW.o states/statesW.o states/bootW.o states/menuW.o states/gameW.o states/closeW.o gameObjects/planetW.o gameObjects/solar_systemW.o gameObjects/starsW.o gameObjects/uiW.o
	x86_64-w64-mingw32-gcc -D_GNU_SOURCE engineW.o utils/toolboxW.o utils/rendererW.o utils/perlin_noiseW.o utils/namegenW.o utils/chunkgenW.o utils/tilemapW.o states/statesW.o states/bootW.o states/menuW.o states/gameW.o states/closeW.o gameObjects/planetW.o gameObjects/solar_systemW.o gameObjects/starsW.o gameObjects/uiW.o -o Odyssey.exe $(WLIBS) $(CLIBS) 

# Compilation du fichier objet principal #
engine.o: engine.c engine.h utils/utils.h states/states.h gameObjects/planet.h gameObjects/solar_system.h
	gcc $(CFLAGS) -c engine.c -o engine.o

engineW.o: engine.c engine.h utils/utils.h states/states.h gameObjects/planet.h gameObjects/solar_system.h
	x86_64-w64-mingw32-gcc $(CFLAGS) -Wl,-subsystem,windows  -c engine.c -o engineW.o

# Compilation des fichiers "utils" #
utils/renderer.o: utils/renderer.c utils/renderer.h
	gcc $(CFLAGS) -c utils/renderer.c -o utils/renderer.o

utils/toolbox.o: utils/toolbox.c utils/toolbox.h
	gcc $(CFLAGS) -c utils/toolbox.c -o utils/toolbox.o

utils/perlin_noise.o: utils/perlin_noise.c utils/utils.h
	gcc $(CFLAGS) -c utils/perlin_noise.c -o utils/perlin_noise.o

utils/namegen.o: utils/namegen.c utils/utils.h
	gcc $(CFLAGS) -c utils/namegen.c -o utils/namegen.o

utils/chunkgen.o: utils/chunkgen.c utils/utils.h
	gcc $(CFLAGS) -c utils/chunkgen.c -o utils/chunkgen.o

utils/tilemap.o: utils/tilemap.c utils/utils.h
	gcc $(CFLAGS) -c utils/tilemap.c -o utils/tilemap.o

# Compilation des fichiers "utils" WINDOWS VERSION #
utils/rendererW.o: utils/renderer.c utils/renderer.h
	x86_64-w64-mingw32-gcc $(CFLAGS) -Wl,-subsystem,windows  -c utils/renderer.c -o utils/rendererW.o

utils/toolboxW.o: utils/toolbox.c utils/toolbox.h
	x86_64-w64-mingw32-gcc $(CFLAGS) -Wl,-subsystem,windows  -c utils/toolbox.c -o utils/toolboxW.o

utils/perlin_noiseW.o: utils/perlin_noise.c utils/utils.h
	x86_64-w64-mingw32-gcc $(CFLAGS) -Wl,-subsystem,windows -c utils/perlin_noise.c -o utils/perlin_noiseW.o

utils/namegenW.o: utils/namegen.c utils/utils.h
	x86_64-w64-mingw32-gcc $(CFLAGS) -Wl,-subsystem,windows  -c utils/namegen.c -o utils/namegenW.o

utils/chunkgenW.o: utils/chunkgen.c utils/utils.h
	x86_64-w64-mingw32-gcc $(CFLAGS) -Wl,-subsystem,windows  -c utils/chunkgen.c -o utils/chunkgenW.o

utils/tilemapW.o: utils/tilemap.c utils/utils.h
	x86_64-w64-mingw32-gcc $(CFLAGS) -Wl,-subsystem,windows  -c utils/tilemap.c -o utils/tilemapW.o


# Compilation des fichiers "states" #
states/states.o: states/states.c utils/utils.h states/states.h
	gcc $(CFLAGS) -c states/states.c -o states/states.o

states/boot.o: states/boot.c utils/utils.h states/states.h
	gcc $(CFLAGS) -c states/boot.c -o states/boot.o

states/menu.o: states/menu.c utils/utils.h states/states.h
	gcc $(CFLAGS) -c states/menu.c -o states/menu.o

states/game.o: states/game.c utils/utils.h states/states.h
	gcc $(CFLAGS) -c states/game.c -o states/game.o

states/close.o: states/close.c utils/utils.h states/states.h
	gcc $(CFLAGS) -c states/close.c -o states/close.o

# Compilation des fichiers "states" WINDOWS VERSION #
states/statesW.o: states/states.c utils/utils.h states/states.h
	x86_64-w64-mingw32-gcc $(CFLAGS) -Wl,-subsystem,windows  -c states/states.c -o states/statesW.o

states/bootW.o: states/boot.c utils/utils.h states/states.h
	x86_64-w64-mingw32-gcc $(CFLAGS) -Wl,-subsystem,windows  -c states/boot.c -o states/bootW.o

states/menuW.o: states/menu.c utils/utils.h states/states.h
	x86_64-w64-mingw32-gcc $(CFLAGS) -c states/menu.c -o states/menuW.o

states/gameW.o: states/game.c utils/utils.h states/states.h
	x86_64-w64-mingw32-gcc $(CFLAGS) -Wl,-subsystem,windows  -c states/game.c -o states/gameW.o

states/closeW.o: states/close.c utils/utils.h states/states.h
	x86_64-w64-mingw32-gcc $(CFLAGS) -Wl,-subsystem,windows  -c states/close.c -o states/closeW.o

# Compilation des fichiers gameObjects #
gameObjects/solar_system.o: gameObjects/solar_system.c utils/utils.h gameObjects/planet.h gameObjects/solar_system.h
	gcc $(CFLAGS) -c gameObjects/solar_system.c -o gameObjects/solar_system.o

gameObjects/planet.o: gameObjects/planet.c utils/utils.h gameObjects/planet.h
	gcc $(CFLAGS) -c gameObjects/planet.c -o gameObjects/planet.o

gameObjects/stars.o: gameObjects/stars.c utils/utils.h gameObjects/stars.h
	gcc $(CFLAGS) -c gameObjects/stars.c -o gameObjects/stars.o

gameObjects/ui.o: gameObjects/ui.c utils/utils.h gameObjects/ui.h
	gcc $(CFLAGS) -c gameObjects/ui.c -o gameObjects/ui.o

# Compilation des fichiers gameObjects WINDOWS VERSION #
gameObjects/solar_systemW.o: gameObjects/solar_system.c utils/utils.h gameObjects/planet.h gameObjects/solar_system.h
	x86_64-w64-mingw32-gcc $(CFLAGS) -Wl,-subsystem,windows  -c gameObjects/solar_system.c -o gameObjects/solar_systemW.o

gameObjects/planetW.o: gameObjects/planet.c utils/utils.h gameObjects/planet.h
	x86_64-w64-mingw32-gcc $(CFLAGS) -Wl,-subsystem,windows  -c gameObjects/planet.c -o gameObjects/planetW.o

gameObjects/starsW.o: gameObjects/stars.c utils/utils.h gameObjects/stars.h
	x86_64-w64-mingw32-gcc $(CFLAGS) -Wl,-subsystem,windows  -c gameObjects/stars.c -o gameObjects/starsW.o

gameObjects/uiW.o: gameObjects/ui.c utils/utils.h gameObjects/ui.h
	x86_64-w64-mingw32-gcc $(CFLAGS) -Wl,-subsystem,windows  -c gameObjects/ui.c -o gameObjects/uiW.o

# Regles de nettoyage du projet #
reset:
	rm exec *.o ./utils/*.o ./states/*.o ./gameObjects/*.o data/saves/*.planmap data/saves/*.solmap

clean:
	rm *.o ./utils/*.o ./states/*.o ./gameObjects/*.o
	