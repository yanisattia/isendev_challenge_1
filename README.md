/**
* Project : ISEN DEV - Challenge N°1
* Author  : ATTIA Yanis
* Purpose : Jeu d'exploration spatiale avec génération procédurale de contenus  
* Version : Alpha 0.1.2
**/  
  
/** TODO LIST **/  
* Gestion des états du jeu (boot, menu, partie, fin) - DONE  
* Création des game objects (planètes, systèmes, joueur) 
    -> gestion des images - DONE  
    -> structure game Object  - WIP  (Player)
    -> Création de systèmes solaires - DONE  
    -> Création d'une carte globale d'un système solaire - TO DO
* Perlin NOISE (génération procédurale de contenu) - DONE  
* Création du gameplay - TO DO
* Génération de terrain en 2D A partir du perlin noise  - DONE
* Génération de chunks de manière infinie - DONE
* Optimisation des performances - DONE (Record : 2000+ fps sans cap)
* Création d'interface graphique - DONE
* Ajouter l'interface du jeu (Vaisseau / Cockpit ?) lors du choix des planètes - TO DO 
* Meilleure gestion des saves - DONE
  
    
/** BUG TRACKING **/  

Bug ID  : 001  
Brief   : Fuite mémoire lors du switch FULLSCREEN <-> WINDOWED. 
LOG     : Lors du switch de mode, il y a rechargement des textures et des éléments d'UI. Ce qui cause le bug.
Fct     : destroyCanvas()
Fix     : Vérifier cette fonction de libération de mémoire mais aussi celle d'allocation.
Fixed   : NOP

Bug ID  : 002
Brief   : Possible crash lors de la création d'une nouvelle partie...
LOG     : Lors de l'initialisation du slot 1 d'une nouvelle partie, segmentation fault (core dumped) peut arriver.
Fct     : Aucune idée
Fix     : Vérifier le reset des saves et la création des nouvelles...
Fixed   : YES - Ver. Alpha 0.1.2
  
  
/** CHANGE LOG **/  
  
Version : 0.0.1_dev  
LOG     : Ajout du starter pack SDL pour avoir une fenêtre graphique et une première structure solide    

Version : 0.0.2_dev  
LOG     : Ajout des fichiers d'état, et d'une toolbox.h  

Version : 0.0.3_dev
LOG     : Ajout de la gestion des images et d'un système de zoom pour les images

Version : 0.0.4_dev
LOG     : Tous les fichiers concernant les différents états du jeu sont en place. Il ne reste plus qu'à coder dans ces fichiers là ce que l'on voudra pour les différents états du programme. Pour passer d'un état à l'autre -> setState(*etat*). Cette fonction permet de passer entre les différents états via une enumeration (0 = BOOT, 1 = MENU, 2 = GAME, 3 = CLOSE)  
  
Version : 0.0.5_dev
LOG     : Création de l'algorithme de bruit de perlin permettant à partir de valeurs aléatoires de bruit (de 0.0 à 1.0) de créer un ensemble de valeurs se suivant et ainsi, créant du sens entre les échantillons : Un premier pas vers la génération procédurale de contenu. Les fonctions pourront être utilisées pour générer du terrain sur les planétes, ou encore, les systèmes solaires eux mêmes peut être. En bref, le bruit de perlin nous sera extrémement utile pour la génération procédurale de contenu !  
  
Version : 0.0.6_dev
LOG     : Structures pour planètes et systèmes solaires, génération aléatoire de systèmes solaires (taille et nom) fonctionnelle. Reste à générer les planètes et les afficher pour avoir un premier début.  

Version : 0.0.7_dev
LOG     : Structure pour les planètes et génération des planètes de manière aléatoire. Reste à gérer leur position dans le système solaire et un affichage simple d'un système serait parfait !  

Version : 0.0.8_dev  
LOG     : Affichage des planètes sur la fenêtre graphique ! INSANE !!!  

Version : Alpha 0.1.0
LOG     : (Beaucoup de temps depuis la dernière MAJ du readme.md) - Gestion de la génération de chunks, sans problèmes, création de terrain 2D à partir du Perlin Noise et ce, de manière infinie. Création de planètes, affichage via textures, sauvegarde et chargement, ainsi qu'accès par le menu principal. 

Version : Alpha 0.1.1
LOG     : Ajout du fps_limit dans le fichier de config renderer.ini

Version : Alpha 0.1.2
LOG     : Correction d'un bug qui faisait planter a l'ouverture de sauvegardes.