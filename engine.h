#ifndef __ENGINE_H
#define __ENGINE_H
/*
 * @file  : engine
 * @brief : The main file for the game engine
*/


#include "utils/utils.h"
#include "gameObjects/ui.h"
#include "gameObjects/solar_system.h"

typedef struct engineT engineT;
/*
 * @struct : engineT
 * @brief  : This structure contains data about the program during its execution
*/
struct engineT{
    bool process; //defines wether or not the program is running
    bool debug; //if debug mode is on, can access some dev features
    int state; //the program state defined here (0 - boot, 1 - menu, 2 - game, 3 - close)
    SDL_Event event; //SDL_Event for the user input and actions
    rendererData *render;

    uiCanvasT ui, new_game, load_game;
    uiCanvasT options_menu;
    int loaded, loading;

    int displayFps;
    double frameCounter;
    vector2 mousePos;
};

/*
 * @func   : init();
 * @params : void
 * @return : void
 * @brief  : This function will contain all the code that
 * must only be ran once at the beggining of the program
*/
void init();

/*
 * @func   : update();
 * @params : void
 * @return : void
 * @brief  : This function will contain all the code that
 * should run multiple times until the program ends
*/
void update(void);

/*
 * @func   : draw();
 * @params : void
 * @return : void
 * @brief  : This function will contain all the code that
 * should draw things in the window until the program ends
*/
void draw();

/*
 * @func   : end();
 * @params : void
 * @return : void
 * @brief  : This function will contain all the code that
 * will be running just before program stops
*/
void end();
void loadingScreen();
/* GAME DATA */
typedef struct gameDataT gameDataT;
//Contient les données du jeu (et de sauvegarde)
struct gameDataT{
    int slot;
    bool empty;

    int nb_systems;
    solarSystemT *systems;
};

gameDataT *createGameData(int slot);
void saveGameData(gameDataT *gameData);
gameDataT *loadGameData(int slot);
void clearGameData(gameDataT *gameData);
void addSolarSystem(gameDataT *gameData);
void loadSystems(gameDataT *gameData);
void resetSaves(int i);

#endif //__ENGINE_H